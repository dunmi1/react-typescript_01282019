import * as React from 'react';
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom';

import { Home, About, Contact } from './components';

import './App.css';

class App extends React.Component {
  public render() {
    return (
      <Router>
        <div>
          <header>
            <h1>My App</h1>
          </header>
          <nav>
            <ul>
              <li><Link to="/">Home</Link></li>
              <li><Link to={{ pathname: '/about', state: { test: 123 }}}>About</Link></li>
              <li><Link to="/contact">Contact</Link></li>
            </ul>
          </nav>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/about" component={About} />
            <Route path="/about" component={About} />
            <Route path="/contact" component={Contact} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
