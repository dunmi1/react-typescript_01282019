import React from 'react';
import ReactDOM from 'react-dom';

export enum CalcActions {
  ADD = '[Calc] Add',
  SUBTRACT = '[Calc] Subtract',
  MULTIPLY = '[Calc] Multiply',
  DIVIDE = '[Calc] Divide',
}
export interface Action<T = any> {
  type: T;
}

export interface AnyAction extends Action {
  [ x: string ]: any
}

export interface CalcAction extends AnyAction {
  payload: number;
}
//                        // type definition of the function
// const createAddAction: (p: number) => CalcAction<string> =
//   (payload: number) => ({ type: CalcActions.ADD, payload }); // implementation of the function

export class CalcAddAction implements CalcAction {
  type = CalcActions.ADD;
  constructor(public payload: number) { }
  // payload: number;
  // constructor(payload: number) {
  //   this.payload = payload;
  // }
}

export class CalcSubtractAction implements CalcAction {
  type = CalcActions.SUBTRACT;
  constructor(public payload: number) { }
}

export class CalcMultiplyAction implements CalcAction {
  type = CalcActions.MULTIPLY;
  constructor(public payload: number) { }
}

export class CalcDivideAction implements CalcAction {
  type = CalcActions.DIVIDE;
  constructor(public payload: number) { }
}

export type CalcActionsUnion = CalcAddAction | CalcSubtractAction | CalcMultiplyAction | CalcDivideAction;

export type State = any;

export interface CalcState extends State {
  result: number;
}

export type Reducer<S = any, A extends Action = AnyAction> = (state: S | undefined, action: A) => S;

// export type Reducer<S extends State> = (s: S, a: Action ) => S;

export const calcReducer: Reducer<CalcState, CalcAction> = ( state: CalcState = { result: 0 }, action: CalcAction ) => {

  const calcAction = action;
  
  switch(action.type) {
    case CalcActions.ADD:
      return { result: state.result + calcAction.payload };
    case CalcActions.SUBTRACT:
      return { result: state.result - calcAction.payload };
    case CalcActions.MULTIPLY:
      return { result: state.result * calcAction.payload };
    case CalcActions.DIVIDE:
      return { result: state.result / calcAction.payload };
    default:
      return state;
  }
};

export type Dispatch<A extends Action> = (action: A) => void;

export interface Store<S = any, A extends Action = AnyAction> {
  getState: () => S;
  dispatch: Dispatch<A>;
  subscribe: (cb: Subscribe) => Unsubscribe;
}

export type Subscribe = () => void;
export type Unsubscribe = () => void;

export interface CreateStore {
  <S, A extends Action>(reducer: Reducer<S, A>): Store<S, A>;
}


export const createStore: CreateStore = <S extends State, A extends Action>(reducer: Reducer<S, A>) => {

  let currentState: S = reducer.apply(null, [ undefined, { type: '@INIT' } as A ]);
  const subscribers: Subscribe[] = [];

  return {
    getState: () => currentState,
    dispatch: (action: A) => {
      currentState = reducer(currentState, action);
      subscribers.forEach(cb => cb());
    },
    subscribe: (cb: Subscribe) => {
      subscribers.push(cb);
      return () => {
        const index = subscribers.findIndex(fn => fn === cb);
        subscribers.splice(index, 1);
      };
    },
  };

};

const store = createStore(calcReducer);

store.subscribe(() => {
  console.log('new state', store.getState());
})

export type ActionsMap = {
  [ actionKey: string ]: (...params: any[]) => AnyAction;
}

export type BoundActionsMap = {
  [ actionKey: string ]: (...params: any[]) => void;
}

// export type Dispatch = (action: Action) => void;

const bindActionCreators = (actionsMap: ActionsMap, dispatch: Dispatch<Action>) => {
  return Object.keys(actionsMap).reduce(
    (boundActions: BoundActionsMap, actionKey: string) => {
      boundActions[actionKey] = (...params: any[]) => dispatch(actionsMap[actionKey](...params));
      return boundActions;
    },
    {},
  ) as BoundActionsMap;
};

interface CalcToolProps {
  result: number;
  onAdd: (value: number) => void;
  onSubtract: (value: number) => void;
  onMultiply: (value: number) => void;
  onDivide: (value: number) => void;
}

interface CalcToolState {
  numInput: number;
}

export class CalcTool extends React.Component<CalcToolProps, CalcToolState> {

  state = {
    numInput: 0,
  }

  change = ({ target: { value }}: React.ChangeEvent<HTMLInputElement>) =>
    this.setState({ numInput: Number(value )});

  render() {
    return <div>
      <div>Result: {this.props.result}</div>
      <div>Input: <input type="number" value={this.state.numInput} onChange={this.change} /></div>
      <button type="button" onClick={() => this.props.onAdd(this.state.numInput)}>+</button>
      <button type="button" onClick={() => this.props.onSubtract(this.state.numInput)}>-</button>
      <button type="button" onClick={() => this.props.onMultiply(this.state.numInput)}>*</button>
      <button type="button" onClick={() => this.props.onDivide(this.state.numInput)}>/</button>
    </div>

  }

}

interface ContainerComponentProps<S, A extends Action> {
  store: Store<S, A>;
}

export type MapStateToProps<S = any> = (state: S) => Object;
export type MapDispatchToProps = (dispatch: Dispatch<AnyAction>) => Object;

const connect = (mapStateToPropsFn: MapStateToProps, mapDispatchToPropsFn: MapDispatchToProps) => {

  return (PresentationalComponent: React.StatelessComponent<any> | React.ComponentClass<any, any>) => {

    return class ContainerComponent<S, A extends Action> extends React.Component<ContainerComponentProps<S,A>> {

      dispatchProps: any;
      storeUnsubscribe: Unsubscribe;

      constructor(props: ContainerComponentProps<S, A>) {
        super(props);

        this.storeUnsubscribe = props.store.subscribe(() => {
          this.forceUpdate();
        });

        this.dispatchProps = mapDispatchToPropsFn(props.store.dispatch as Dispatch<AnyAction>);
      }

      componentWillUnmount() {
        this.storeUnsubscribe();
      }

      render() {
        return <PresentationalComponent
          {...this.dispatchProps}
          {...mapStateToPropsFn(this.props.store.getState())} />;
      }
    };
  };
};

const createCalcToolContainer = connect(
  (state: CalcState) => ({ result: state.result }), // map state to props
  (dispatch: Dispatch<Action>) => bindActionCreators({
    onAdd: (value: number) => new CalcAddAction(value),
    onSubtract: (value: number) => new CalcSubtractAction(value),
    onMultiply: (value: number) => new CalcMultiplyAction(value),
    onDivide: (value: number) => new CalcDivideAction(value),
  }, dispatch), // map dispatch to props
);

const CalcToolContainer = createCalcToolContainer(CalcTool);


ReactDOM.render(<CalcToolContainer store={store} />, document.querySelector('#root'));


