import React from 'react';

import toolHeaderCSS from './ToolHeader.module.css';
interface ToolHeaderProps {
  headerText: string;
}

export const ToolHeader = React.memo(({ headerText }: ToolHeaderProps) => {

  console.log('rendered tool header');

  return <header>
    <h1 className={toolHeaderCSS['page-header-text']}>{headerText}</h1>
    <span>Cool</span>
  </header>;
});