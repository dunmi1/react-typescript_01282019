import * as React from 'react';
import * as ReactDOM from 'react-dom';

export interface Action {
  type: string;
}

export type State = any;

export type Reducer<S extends State, A extends Action> =
  (state: S, action: A) => S;


enum CalcActions {
  ADD = 'ADD',
  SUBTRACT = 'SUBTRACT',
  MULTIPLY = 'MULTIPLY',
}

class AddAction implements Action {
  public readonly type = CalcActions.ADD;
  constructor(public payload: number) { }
}

class SubtractAction implements Action {
  public readonly type = CalcActions.SUBTRACT;
  constructor(public payload: number) { }
}

class MultiplyAction implements Action {
  public readonly type = CalcActions.MULTIPLY;
  constructor(public payload: number) { }
}


type CalcActionsUnion = AddAction | SubtractAction | MultiplyAction;

type AppState = number;

const calcReducer: Reducer<AppState, CalcActionsUnion> = (state: AppState = 0, action: CalcActionsUnion) => {
  console.log('state', state, 'action', action);

  switch(action.type) {
    case CalcActions.ADD:
      return state + action.payload;
    case CalcActions.SUBTRACT:
      return state - action.payload;
    case CalcActions.MULTIPLY:
    return state * action.payload;
    default:
      return state;
  }

};

type Subscriber = () => void;
type Unsubscribe = () => void;

interface Store<S extends State> {
  getState: () => S;
  dispatch: (action: Action) => void;
  subscribe: (fn: Subscriber) => Unsubscribe;
}

type CreateStore = <S, A extends Action>(reducer: Reducer<S, A>) => Store<S>;

const createStore: CreateStore = <S, A extends Action>(reducer: Reducer<S, A>) => {

  let currentState: S;
  const subscribers: Subscriber[] = [];

  // is to trigger the initial rendering of the component
  // setTimeout(() => {
  //   currentState = reducer.apply(null, [undefined, { type: '@INIT' }]);
  //   subscribers.forEach(fn => fn());
  // }, 0);
  
  return {
    getState: () => currentState,
    dispatch: (action: any) => {
      currentState = reducer(currentState, action);
      subscribers.forEach(fn => fn());
    },
    subscribe: (fn: Subscriber) => {
      subscribers.push(fn);
      return () => {
        const index = subscribers.findIndex(fn2 => fn2 === fn);
        subscribers.splice(index, 1);
      };
    },
  };

};

const store: Store<AppState> = createStore(calcReducer);

interface ActionMap {
  [x: string]: (...params: any[]) => Action;
}

type BoundActionMap = {
  [x: string]: (...params: any[]) => void;
};

type Dispatch = (action: Action) => void; 

const bindActionCreators = (actions: ActionMap, dispatch: Dispatch) => {
  return Object.keys(actions).reduce( (boundActions: BoundActionMap, actionKey) => {
    boundActions[actionKey] = (...params: any[]) => dispatch( actions[actionKey](...params) );
    return boundActions;
  }, {} as BoundActionMap);
};

const { add, subtract, multiply } = bindActionCreators({
  add: (num: number) => new AddAction(num),
  subtract: (num: number) => new SubtractAction(num),
  multiply: (num: number) => new MultiplyAction(num),
}, store.dispatch);  


// Task: build a CalcTool component in this file which functions as described on the board.
// Use Redux to manage the result state and process the calculation button operations.
// You are adding multiply and divide.

interface CalcToolProps {
  result: number;
  onAdd: (num: number) => void;
  onSubtract: (num: number) => void;
  onMultiply: (num: number) => void;
};

interface CalcToolState {
  input: number;
}

class CalcTool extends React.Component<CalcToolProps, CalcToolState> {

  constructor(props: CalcToolProps) {
    super(props);

    this.state = {
      input: 0,
    };
  }

  public change = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ input: Number(e.target.value) });
  }

  public render() {

    const { result, onAdd, onSubtract, onMultiply } = this.props;

    return <form>
      <div>
        Result: {result}
      </div>
      <div>
        Input: <input type="number" value={this.state.input} onChange={this.change} />
      </div>
      <button type="button" onClick={() => onAdd(this.state.input)}>+</button>
      <button type="button" onClick={() => onSubtract(this.state.input)}>-</button>
      <button type="button" onClick={() => onMultiply(this.state.input)}>*</button>
    </form>;
  }
}

store.subscribe(() => {
  // You will need to specify the props which are needed CalcTool
  ReactDOM.render(
    <CalcTool result={store.getState()} onAdd={add} onSubtract={subtract} onMultiply={multiply} />,
    document.querySelector('#root'),
  );
});


// store.dispatch(new AddAction(1));

