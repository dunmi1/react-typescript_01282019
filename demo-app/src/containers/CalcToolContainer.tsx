import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from 'redux';

import { CalcState } from '../models/CalcState';
import { CalcAddAction, CalcDivideAction, CalcMultiplyAction, CalcSubtractAction } from '../actions/calc.actions';
import { CalcTool } from '../components/CalcTool';

const createCalcToolContainer = connect(
  (state: CalcState) => ({ result: state.result }), // map state to props
  (dispatch: Dispatch) => bindActionCreators({
    onAdd: (value: number) => new CalcAddAction(value),
    onSubtract: (value: number) => new CalcSubtractAction(value),
    onMultiply: (value: number) => new CalcMultiplyAction(value),
    onDivide: (value: number) => new CalcDivideAction(value),
  }, dispatch), // map dispatch to props
);

export const CalcToolContainer = createCalcToolContainer(CalcTool);