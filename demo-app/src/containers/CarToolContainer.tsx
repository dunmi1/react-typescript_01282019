import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';

import { CarToolState } from '../models/CarToolState';
import { CarTool } from '../components/CarTool';
import { actions, refreshCars } from '../actions/car.actions';

const mapStateToProps = ({ cars, editCarId }: CarToolState) => ({ cars, editCarId });
const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
  onRefresh: refreshCars,
  onAppend: actions.appendCar,
  onReplace: actions.replaceCar,
  onEdit: actions.editCar,
  onDelete: actions.deleteCar,
  onCancel: actions.cancelCar,
}, dispatch);

export const CarToolContainer = connect(mapStateToProps, mapDispatchToProps)(CarTool);