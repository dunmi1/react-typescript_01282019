import * as React from 'react';
import * as renderer from 'react-test-renderer';
import { render, configure } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';

import { HelloWorld } from './hello-world';

configure({ adapter: new Adapter() });

describe('<HelloWorld /> React Test Renderer Static HTML', () => {

  it('<HelloWorld /> renders', () => {

    const tree = renderer.create(<HelloWorld />).toJSON();
    expect(tree).toMatchSnapshot();

  });

});

describe('<HelloWorld /> Enzyme Static HTML', () => {

  it('<HelloWorld /> renders', () => {
    const component = JSON.stringify(render(<HelloWorld />).html());
    expect(component).toMatchSnapshot();
  });

});
