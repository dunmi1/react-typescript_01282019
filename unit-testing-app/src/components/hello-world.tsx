import * as React from 'react';

export class HelloWorld extends React.Component {

  public render() {
    return <h1>Hello World!</h1>;
  }
}
