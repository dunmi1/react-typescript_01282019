import * as React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import { shallow, configure, ShallowWrapper } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';

import { HelloWorld } from './hello-world';

configure({ adapter: new Adapter() });

describe('<HelloWorld /> Shallow with React Test Renderer', () => {

  let component: any;

  beforeEach(() => {
    const renderer = createRenderer();
    renderer.render(<HelloWorld />);
    component = renderer.getRenderOutput();
  });

  test('<HelloWorld /> renders', () => {
    expect(component.props.children).toBe('Hello World!');
  });

});

describe('<HelloWorld /> Shallow with Enzyme', () => {

  let component: ShallowWrapper;

  beforeEach(() => {
    component = shallow(<HelloWorld />);
  });

  test('<HelloWorld /> renders', () => {
    expect((component.props() as { children: any }).children).toBe('Hello World!');
  });

});