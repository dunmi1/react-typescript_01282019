import * as React from 'react';

export interface Props {
  color: string;
  buttonText: string;
  onSubmitColor: (color: string) => void;
}

export interface State {
  color: string;
  [ x: string ]: any;
}

export class ColorForm extends React.Component<Props, State> {

  public static defaultProps = {
    color: '',
    buttonText: 'Submit Color',
  };

  public colorInput: HTMLInputElement | null;

  constructor(props: Props) {
    super(props);

    this.state = {
      color: props.color,
    };
  }

  public onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [ e.target.name ]: e.target.value,
    });
  }

  public submitColor = () => {
    this.props.onSubmitColor(this.state.color);

    this.setState({
      color: '',
    });
  }

  public componentDidMount() {
    if (this.colorInput) {
      this.colorInput.focus();
    }
  }

  public render() {
    return <form>
      <div>
        <label htmlFor="color-input">Color:</label>
        <input type="text" id="color-input" name="color" ref={ input => this.colorInput = input }
          value={this.state.color} onChange={this.onChange} />
      </div>
      <button type="button" onClick={this.submitColor}>{this.props.buttonText}</button>
    </form>;
  }
}
