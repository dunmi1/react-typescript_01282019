import * as React from 'react';

import { Widget } from '../models/Widget';

export interface Props {
  widget: Widget;
  onDeleteWidget: (widgetId: number) => void;
  onEditWidget: (widgetId: number) => void;
}

export const WidgetViewRow = ({ widget, onDeleteWidget, onEditWidget }: Props) =>
  <tr>
    <td>{widget.id}</td>
    <td>{widget.name}</td>
    <td>{widget.description}</td>
    <td>{widget.color}</td>
    <td>{widget.size}</td>
    <td>{widget.price}</td>
    <td>{widget.quantity}</td>
    <td>
      <button type="button" onClick={() => onEditWidget(widget.id)}>Edit</button>
      <button type="button" onClick={() => onDeleteWidget(widget.id)}>Delete</button>
    </td>
  </tr>;

